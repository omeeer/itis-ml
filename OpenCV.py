import cv2

if __name__ == "__main__":
    cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
    image = cv2.imread('photo.jpg')

    dimensions = (int(image.shape[1]), int(image.shape[0]))

    gray_scaled_color = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    for (x, y, width, height) in cascade.detectMultiScale(gray_scaled_color, 1.1, 12):
        cv2.rectangle(image, (x, y), (x + width, y + height), (0, 0, 255), 2)

    cv2.imshow('output', image)
    cv2.waitKey()

