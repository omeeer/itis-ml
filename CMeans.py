import matplotlib.pyplot as plt
import numpy as np


def find_dist(a, b):
    return np.sqrt((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2)


def generate_points(n):
    points = []
    for i in range(n):
        points.append([np.random.randint(0, 100), np.random.randint(0, 100)])
    return points


def find_radius(n):
    x_sum, y_sum, R = 0, 0, 0
    points = generate_points(n)
    for i in range(n):
        x_sum += points[i][0]
        y_sum += points[i][1]
    circle_center = [x_sum / len(points), y_sum / len(points)]
    for i in range(len(points)):
        R = max(R, find_dist(circle_center, points[i]))
    return R, circle_center, points


def find_centroids_for_circle(R, k, x_sum, y_sum):
    centroids = []
    for i in range(k):
        centroids.append([x_sum + R * np.cos(2 * np.pi * i / k), y_sum + R * np.sin(2 * np.pi * i / k)])
    return centroids


def set_points_to_cluster(points, centroids):
    cluster_points = [[] for i in range(len(centroids))]
    for i in range(len(points)):
        min = 100
        j_min = 0
        for j in range(len(centroids)):
            dist = find_dist(points[i], centroids[j])
            if dist < min:
                min = dist
                j_min = j
        cluster_points[j_min].append(points[i])
    return cluster_points


def set_matrix(m, points, centroids):
    matrix = [[] for i in range(len(centroids))]
    for i in range(len(points)):
        for j in range(len(centroids)):
            second = 0
            first = find_dist(points[i], centroids[j]) ** (1 / 1 - m)
            for k in range(len(centroids)):
                second += find_dist(points[i], centroids[k]) ** (1 / 1 - m)
            dist = first / second
            matrix[j].append(dist)
    return matrix


def find_centroids(matrix, k, m, points):
    centroids = []
    for i in range(k):
        x_sum, y_sum, prob = 0, 0, 0
        for j in range(len(points)):
            x_sum += matrix[i][j] ** m * points[i][0]
            y_sum += matrix[i][j] ** m * points[i][1]
            prob += matrix[i][j] ** m
        center = [(x_sum / prob), (y_sum / prob)]
        centroids.append(center)
    return centroids


def find_max_in_matrix(matrix1, matrix2):
    res = 0
    for i in range(len(matrix1)):
        for j in range(len(matrix1)):
            res = max(res, abs(matrix2[i][j] - matrix1[i][j]))
    return res


def set_points_to_cluster_via_prob(matrix, points):
    cluster_points = [[] for i in range(len(matrix))]
    for i in range(len(matrix[0])):
        max = 0
        j_max = 0
        for j in range(len(matrix)):
            if matrix[j][i] > max:
                max = matrix[j][i]
                j_max = j
        cluster_points[j_max].append(points[i])
    return cluster_points


def plot(centroids, cluster_points):
    color_list = ['g', 'y', 'c', 'm', 'g', 'k']
    for k in range(len(centroids)):
        plt.scatter([i[0] for i in cluster_points[k]], [i[1] for i in cluster_points[k]], color=color_list[k])
    plt.scatter([i[0] for i in centroids], [i[1] for i in centroids], color='r')
    ax = plt.axes()
    ax.set_facecolor("black")
    plt.axis('scaled')
    plt.draw()
    plt.show()


def c_means(_n, k, m, eps):
    res = find_radius(_n)
    R = res[0]
    center = res[1]
    points = res[2]

    centroids_for_circle = find_centroids_for_circle(R, k, center[0], center[1])
    cluster_points = set_points_to_cluster(points, centroids_for_circle)
    circle = plt.Circle((center[0], center[1]), R, color='r', fill=False)
    ax = plt.gca()
    ax.add_patch(circle)
    circle = plt.Circle((center[0], center[1]), R, color='w', fill=False)
    ax = plt.gca()
    ax.add_patch(circle)
    plot(centroids_for_circle, cluster_points)
    print('centroids = {}, clustered-points = {}'.format(centroids_for_circle, cluster_points))

    matrix = set_matrix(m, points, centroids_for_circle)
    centroids = find_centroids(matrix, k, m, points)
    new_cluster_points = set_points_to_cluster_via_prob(matrix, points)
    circle = plt.Circle((center[0], center[1]), R, color='w', fill=False)
    ax = plt.gca()
    ax.add_patch(circle)
    plot(centroids, new_cluster_points)
    print('matrix = {} \ncentroids = {} \nclustered-points = {}'.format(matrix, centroids_for_circle, cluster_points))

    matrix_list = []
    matrix_list.append(matrix)

    for i in range(1000):
        matrix = set_matrix(m, points, centroids)
        centroids = find_centroids(matrix, k, m, points)
        new_cluster_points = set_points_to_cluster_via_prob(matrix, points)
        circle = plt.Circle((center[0], center[1]), R, color='w', fill=False)
        ax = plt.gca()
        ax.add_patch(circle)
        plot(centroids, new_cluster_points)
        matrix_list.append(matrix)
        elements_sum = sum(matrix[i][0] for i in range(len(matrix_list)))
        print('matrix = {} \nsum = {} \ncentroids = {} \nclustered-points = {}'.format(matrix, elements_sum,
                                                                                       centroids_for_circle,
                                                                                       cluster_points))

        max_in_matrix = find_max_in_matrix(matrix_list[i], matrix_list[i + 1])
        print('max_in_matrix =', max_in_matrix)

        if max_in_matrix < eps:
            break


if __name__ == "__main__":
  n = 500
  k = 3
  m = 2
  eps = 0.01
  c_means(n, k, m, eps)