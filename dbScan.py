from math import sqrt, inf
import pygame
from pygame.locals import *
import random
import math
import sys
import itertools


YELLOW = (255, 255, 0)
GREEN = (0, 255, 0)
RED = (255, 0, 0)


ORANGE = (243, 165, 28)
GREENY_GREEN = (194, 243, 28)
PURPLE_LIGHT = (132, 42, 202)
BLUE = (0, 150, 255)
WHITE = (255, 255, 255)
PINKY_PINK = (232, 98, 159)
BLUE_LIGHT = (0, 191, 255)
PURPLE = (102, 42, 202)
PINK = (232, 98, 214)
BLUE_2 = (42, 202, 192)
GREEN_LIGHT = (42, 202, 124)

BLACK = (0, 0, 0)

pygame.init()
pywindow = pygame.display.set_mode((500, 500))
pygame.display.set_caption('Draw circle at cursor')
pywindow.fill(WHITE)

min_neigbors = 3
point_dist = 20


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.color = RED


def find_new_coor(x, y, _R):
    x_new = x + math.sin(random.uniform(0, 2 * math.pi)) * _R
    y_new = y + math.cos(random.uniform(0, 2 * math.pi)) * _R
    return x_new, y_new


def dist(point_a, point_b):
    return sqrt((point_a.x - point_b.x) ** 2 + (point_a.y - point_b.y) ** 2)


def set_green_points(_local_points, _green_points):
    for local_point in _local_points:
        neighbour_count = 0
        for point in points:
            if local_point == point:
                continue
            if dist(local_point, point) <= point_dist:
                neighbour_count += 1
        if neighbour_count >= min_neigbors:
            local_point.color = GREEN
            _green_points.append(local_point)


def remove_points(_local_points, _points):
    for point in _points:
        _local_points.remove(point)


def set_yellow_points(_local_points, _yellow_points, _dic):
    for local_point in _local_points:
        min_dist = inf
        closest = None
        for point in points:
            if local_point == point:
                continue
            if point.color == GREEN:
                if dist(local_point, point) > point_dist:
                    continue
                current_dist = dist(local_point, point)
                if current_dist <= min_dist:
                    min_dist = current_dist
                    closest = point
        if closest is not None:
            local_point.color = YELLOW
            _yellow_points.append(local_point)
            _dic.setdefault(closest, []).append(local_point)


def set_clusters(_green_points, _dic, _clusters):
    colors = itertools.cycle([ORANGE, PURPLE_LIGHT, BLUE, PINKY_PINK, GREEN_LIGHT, PURPLE, PINK, BLUE_2])
    while len(_green_points) > 0:
        cluster = [_green_points.pop(0)]
        for green_point_1 in cluster:
            for green_point_2 in _green_points:
                if green_point_1 == green_point_2:
                    continue
                if dist(green_point_1, green_point_2) <= point_dist:
                    if green_point_2 not in cluster:
                        _green_points.remove(green_point_2)
                        cluster.append(green_point_2)
        yellows_in_cluster = []
        color = next(colors)
        for green_point in cluster:
            green_point.color = color
            if green_point in _dic:
                yellows_in_cluster.extend(_dic[green_point])
        cluster.extend(yellows_in_cluster)
        _clusters.append(cluster)


def dbscan(points):
    local_points = [* points]
    green_points = []
    yellow_points = []
    dic = {}
    clusters = []

    set_green_points(local_points, green_points)
    remove_points(local_points, green_points)

    set_yellow_points(local_points, yellow_points, dic)
    remove_points(local_points, yellow_points)

    set_clusters(green_points, dic, clusters)

    return clusters


if __name__ == "__main__":
    points = []
    sc = 20
    while True:
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                x_point, y_point = pygame.mouse.get_pos()
                points.append(Point(x_point, y_point))

                n = random.choice([2, 5])
                for i in range(n):
                    x_created_point, y_created_point = find_new_coor(x_point, y_point, sc)
                    points.append(Point(x_created_point, y_created_point))

                clusters = dbscan(points)

                print(len(clusters))
                for i in range(len(clusters)):
                    for j in range(len(clusters[i])):
                        pygame.draw.circle(pywindow, clusters[i][j].color, (clusters[i][j].x, clusters[i][j].y), 1, 1)
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
        pygame.display.update()
